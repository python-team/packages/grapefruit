grapefruit (0.1~a4+dfsg-2) unstable; urgency=medium

  * Team Upload.
  * remove extraneous dependency on python3-future (Closes: #1058988)
  * allow building source after successful build (Closes: #1044677)

 -- Alexandre Detiste <tchet@debian.org>  Mon, 01 Jan 2024 20:34:42 +0100

grapefruit (0.1~a4+dfsg-1) unstable; urgency=medium

  * New upstream release
  * Update debian/watch file
  * Remove patches that are no longer required

 -- Jonathan Carter <jcc@debian.org>  Fri, 21 Jan 2022 12:19:03 +0200

grapefruit (0.1~a3+dfsg-10) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/changelog: Remove trailing whitespaces.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Bump debhelper from old 12 to 13.
    + Drop check for DEB_BUILD_OPTIONS containing "nocheck", since debhelper now
      does this.

  [ Jonathan Carter ]
  * Update standards version to 4.6.0
  * Declare Rules-Requires-Root: no

 -- Jonathan Carter <jcc@debian.org>  Mon, 06 Sep 2021 11:38:18 +0200

grapefruit (0.1~a3+dfsg-8) unstable; urgency=medium

  * Add Breaks+Replaces for python-grapefruit-doc
    (Closes: #934599)
  * Remove suggestion for python-grapefruit-doc

 -- Jonathan Carter <jcc@debian.org>  Tue, 20 Aug 2019 07:58:45 +0000

grapefruit (0.1~a3+dfsg-7) unstable; urgency=medium

  [ Ondřej Nový ]
  * Convert git repository from git-dpm to gbp layout.
  * Use debhelper-compat instead of debian/compat.

  [ Jonathan Carter ]
  * Update standards version to 4.4.0
  * Upgrade debhelper-compat to 12
  * Drop python2 binary package
  * Add dependency for python3-future (Closes: #926414)
  * Drop separate python-grapefruit-doc package

 -- Jonathan Carter <jcc@debian.org>  Wed, 07 Aug 2019 11:59:18 +0200

grapefruit (0.1~a3+dfsg-6) unstable; urgency=medium

  * Remove README from python-grapefruit-doc.docs
    (Closes: #914970)

 -- Jonathan Carter <jcc@debian.org>  Sat, 01 Dec 2018 18:46:21 +0200

grapefruit (0.1~a3+dfsg-5) unstable; urgency=medium

  * Fix short package description for python3 version
    (Closes: #910747)
  * Add patch to remove reference to removed GrapeFruit.png
    (Closes: #719488)

 -- Jonathan Carter <jcc@debian.org>  Tue, 27 Nov 2018 10:28:28 +0200

grapefruit (0.1~a3+dfsg-4) unstable; urgency=medium

  [ Vincent Danjean ]
  * add building of python3 modules (Closes: #903477)
  * switch to pybuild Debian build system

  [ Jonathan Carter ]
  * Improve indentation in watch file

 -- Jonathan Carter <jcc@debian.org>  Mon, 01 Oct 2018 14:10:42 +0200

grapefruit (0.1~a3+dfsg-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove trailing whitespaces
  * Convert git repository from git-dpm to gbp layout

  [ Jonathan Carter ]
  * debian/changelog: Remove duplicate copyright field
  * Update standards version to 4.2.1
  * Use python3-sphinx instead of python-sphinx

 -- Jonathan Carter <jcc@debian.org>  Wed, 26 Sep 2018 09:04:52 +0200

grapefruit (0.1~a3+dfsg-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/watch: Use https protocol

  [ Jonathan Carter ]
  * debian/control:
    - Adopting package (Closes: #889781)
    - Update debhelper/compat to level 11
    - Update standards version to 4.1.4
    - Drop obsolete versioned dependencies
  * debian/python-grapefruit-doc.docs:
    - Fix doc-base paths

 -- Jonathan Carter <jcc@debian.org>  Sun, 29 Apr 2018 12:12:46 +0200

grapefruit (0.1~a3+dfsg-1) unstable; urgency=low

  [ Simon Chopin ]
  * Initial release. (Closes: #706042)
  * Do not ship the logo as its license situation is unclear.

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

 -- Simon Chopin <chopin.simon@gmail.com>  Mon, 12 Aug 2013 11:00:26 +0200
